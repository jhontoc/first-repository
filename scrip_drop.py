import sys
import time
import select
import paramiko
import getpass 
import cmd
import datetime
#import json
import re
#from openpyxl import Workbook
#from openpyxl import load_workbook

#reading external files


f1 = open('device_copp', 'r') 
devices = f1.readlines()
f2 = open('commands_copp', 'r') 
commands = f2.readlines()

#wb2 = load_workbook('sample.xlsx')
#sheet=wb2.active
#data=['10.88.255.220']
#if wb2.sheetnames[0]=='EQUIPOS':
#    max_row=sheet.max_row
#    print ('the sheet exist')
#    ws=wb2['EQUIPOS']
#    for d in range(1,max_row+1):
#         data.append(ws['A'+str(d)].value)
#else:
#    print("the sheet doesn't exit")


for eq in devices:
 try:
   print ("Trying to connect to %s" % eq)
   ssh = paramiko.SSHClient()
   ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
   ssh.connect(eq , username = 'jtocaspa', password = 'eLBozivF@A')
   ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
   chan = ssh.invoke_shell()

   chan.send('\n')
   chan.send('terminal length 0 \n')
   time.sleep(1)
   resp = chan.recv(9999)
   output = resp.decode('ascii').split(',')
   chan.send('show clock \n')
   time.sleep(5)
   
#   for command in commands:
   
   chan.send( 'sh ver \n' )
   time.sleep(2)
   chan.send( 'show policy-map interface control-plane \n' )
   time.sleep(2)
   chan.send( 'show system internal copp info \n' )
   time.sleep(2)
   chan.send( 'show system internal access-list input entries det \n' )
   time.sleep(2)
   chan.send( 'show hardware rate-limiter \n' )
   time.sleep(2)
   chan.send( 'show ip arp vrf all \n' )
   time.sleep(2)
   chan.send( 'show ip route summary \n' )
   time.sleep(2)
   chan.send( 'show interface counter errors \n' )
   time.sleep(2)
   chan.send( 'show hardware internal cpu-mac inband counters \n' )
   time.sleep(2)
   chan.send( 'show hardware internal cpu-mac inband stats  \n' )
   time.sleep(2)
   chan.send( 'show system inband queuing statistics \n' )
   time.sleep(2)
   chan.send( 'show system resources \n' )
   time.sleep(2)
   chan.send( 'show system internal pktmgr client \n' )
   time.sleep(2)
   
   buff = b""
   while chan.recv_ready():
       resp1 = chan.recv(999999)
       buff += resp1
       time.sleep(1)
   ssh.close()
   output = buff.decode('ascii').split(',')
   output = ''.join(output)
   date=datetime.datetime.now()
   f=open("%s_" % eq + str(date.day),'w+')
   f.write(output)
   f.close()
 except paramiko.AuthenticationException:
   print ("Authentication failed when connecting to %s" % eq)

 except ConnectionAbortedError:
   print ("Authentication failed when connecting to %s" % eq)
 except TimeoutError:
   print ("TimeoutError %s" % eq)
